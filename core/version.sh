#!/bin/bash
function linuxb_version() {
    echo "Linuxb CLI v1.10.0"
    echo ""
    echo -e "Written by ${DOLPHIN} Thibault Charrin and others; see"
    echo "https://gitlab.com/devops-collab/linuxb"
    return 0
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ./unicode-emojis.sh
# shellcheck source=/dev/null
. ./os-check.sh
linuxb_version
