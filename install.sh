#!/bin/bash

# gather scripts from sub-directories
CORE=("$(find core/*)")
DEV=("$(find dev/*)")
DEVOPS=("$(find devops/*)")
SYSADMIN=("$(find sysadmin/*)")
ZSH=("$(find zsh/*)")

ALL_SCRIPTS=(
    "${CORE[@]}"
    "${DEV[@]}"
    "${DEVOPS[@]}"
    "${SYSADMIN[@]}"
    "${ZSH[@]}"
)

# clean existing temp* and linuxb* files
if [[ -f temp_functions ]]; then rm temp*; fi
if [[ -f linuxb ]]; then rm linuxb*; fi

# generate new linuxb
cp main.sh linuxb
chmod 755 linuxb

# generate temp_pkg_arrays file
{
    echo -e "\nDEV=(${DEV[*]})"
    echo -e "\nDEVOPS=(${DEVOPS[*]})"
    echo -e "\nSYSADMIN=(${SYSADMIN[*]})"
    echo -e "\nZSH=(${ZSH[*]})"
} >>temp_pkg_arrays

# generate temp_functions file removing SHEBANG and REGION at EOF
for PKG in ${ALL_SCRIPTS[*]}; do
    sed <"${PKG}" 's|#!/bin/bash||g' | sed '/#REGION_REMOVED_BY_BUILD_SH/Q' >>temp_functions
done

# inject elements in linuxb
sed -i "/\${TEMP_PKG_ARRAYS}/ {
    r temp_pkg_arrays
    d
    }" linuxb

sed -i "/\${TEMP_FUNCTIONS}/ {
    r temp_functions
    d
    }" linuxb

# create uninstaller
{
    echo "#!/bin/bash"
    echo
    echo "sudo rm -v /usr/local/bin/linuxb*"
    echo -e "echo -e \"\nLinuxb CLI was successfully uninstalled.\""
} >>linuxb-uninstall
chmod 755 linuxb-uninstall

# install linuxb cli
sudo cp -v linuxb linuxb-uninstall /usr/local/bin/
rm temp* linuxb*
echo

linuxb v
echo -e "\nSuccessfully installed."
