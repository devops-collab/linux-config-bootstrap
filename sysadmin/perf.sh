#!/bin/bash
function linuxb_perf() {
    echo -e "\n${PENGUIN} Performance Monitoring Tools (nmon sar) "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Performance Monitoring Tools (nmon sar) "
        echo "https://roadmap.sh/devops"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            echo "macOS is not yet supported"
            return 1
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v nmon >/dev/null ||
            ! command -v sar >/dev/null; then
            case $(grep "^ID=" /etc/os-release | cut -d "=" -f 2 | tr -d '"') in
            "ubuntu" | "debian")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} nmon sysstat
                ;;
            "fedora" | "ol")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} nmon sysstat
                ;;
            "arch")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} nmon sysstat
                ;;
            "opensuse-tumbleweed")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} nmon sysstat
                ;;
            *)
                echo "Generic installation not yet supported"
                return 1
                ;;
            esac
        else
            echo "Already present "
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_perf "$@"
