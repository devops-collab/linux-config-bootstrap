#!/bin/bash
function linuxb_unicode_emojis {
    ROCKET='\U1F680'
    COFFEE='\U2615'
    DOLPHIN='\U1F42C'
    HATCHING='\U1F423'
    LEAF='\U1F331'
    ELEPHANT='\U1F418'
    PYTHON='\U1F40D'
    OCTOPUS='\U1F419'
    WHALE='\U1F433'
    DRAGON='\U1F432'
    BUFFALO='\U1F403'
    WAVE='\U1F30A'
    TIGER='\U1F42F'
    UNICORN='\U1F984'
    PENGUIN='\U1F427'
    return 0
}
#REGION_REMOVED_BY_BUILD_SH
linuxb_unicode_emojis

# Resources
# https://unicode.org/emoji/charts/full-emoji-list.html
