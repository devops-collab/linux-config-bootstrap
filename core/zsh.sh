#!/bin/bash
function linuxb_zsh() {
    echo -e "\n${UNICORN} Install Zsh and Oh My Zsh "

    case $2 in
    "--help" | "help" | "-h" | "h")
        echo
        echo "Usage: linuxb zsh [OPTION]"
        echo
        echo "  -h, --help    display this help and exit"
        echo
        echo "Sources:"
        echo "https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH"
        echo "https://github.com/ohmyzsh/ohmyzsh"
        ;;
    *)
        linuxb_os_check

        echo -e "\n${UNICORN} Checking Zsh "
        if ! command -v zsh >/dev/null; then
            ${LINUXB_PKG_UPDATE}
            ${LINUXB_PKG_INSTALL} zsh
            chsh -s "$(command -v zsh)" "${USER}"
        else
            echo "Already present "
        fi

        echo -e "\n${UNICORN} Checking Oh My Zsh "
        if [[ ! -d ~/.oh-my-zsh ]]; then
            # Before_Script
            # curl
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi
            # Script
            sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
        else
            echo "Already present "
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ./unicode-emojis.sh
# shellcheck source=/dev/null
. ./os-check.sh
linuxb_zsh "$@"
