#!/bin/bash
function linuxb_operator_sdk() {
    echo -e "\n${OCTOPUS} Operator-SDK "
    OPERATOR_VERSION=v1.37.0

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Operator-SDK "
        echo "https://sdk.operatorframework.io/docs/installation/"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            ${LINUXB_PKG_INSTALL} operator-sdk
            return 0
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v operator-sdk >/dev/null; then
            # Before_Script
            # curl
            linuxb_os_check
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi
            # Script
            export ARCH=$(case $(uname -m) in x86_64) echo -n amd64 ;; aarch64) echo -n arm64 ;; *) echo -n $(uname -m) ;; esac)
            export OS=$(uname | awk '{print tolower($0)}')

            export OPERATOR_SDK_DL_URL=https://github.com/operator-framework/operator-sdk/releases/download/$OPERATOR_VERSION
            curl -L -o ~/operator-sdk_${OS}_${ARCH} ${OPERATOR_SDK_DL_URL}/operator-sdk_${OS}_${ARCH}

            chmod +x ~/operator-sdk_${OS}_${ARCH} && sudo mv ~/operator-sdk_${OS}_${ARCH} /usr/local/bin/operator-sdk
        else
            echo "Already present "
            operator-sdk version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_operator_sdk "$@"
