#!/bin/bash
function linuxb_wsl_systemd() {
    echo -e "\n${PENGUIN} Enable Systemd for WSL2 "

    case $2 in
    "--help" | "help" | "-h" | "h")
        echo
        echo "Usage: linuxb wsl [OPTION]"
        echo
        echo "Options:"
        echo
        echo "  -h, --help     display this help and exit"
        echo
        echo "Sources:"
        echo "https://devblogs.microsoft.com/commandline/systemd-support-is-now-available-in-wsl/"
        echo "https://github.com/microsoft/WSL/issues/9887#issuecomment-1516434388"
        ;;
    *)
        if [[ $(uname -a | grep -c WSL2) == 0 ]]; then
            echo "Kernel is not running on WSL2, you may not run this patch."
            return 1
        fi
        if [[ ! -f /etc/wsl.conf ]] || [[ "$(grep -c "systemd=true" /etc/wsl.conf)" == 0 ]]; then
            echo -e "[boot]\nsystemd=true" | sudo tee -a /etc/wsl.conf
        else
            echo -e "\nwsl.conf was already patched! ${PENGUIN} "
        fi

        if [[ ! -d /usr/lib/binfmt.d/ ]]; then sudo mkdir -pv /usr/lib/binfmt.d/; fi

        if [[ ! -f /usr/lib/binfmt.d/WSLInterop.conf ]] ||
            [[ "$(grep -c ":WSLInterop:M::MZ::/init:PF" /usr/lib/binfmt.d/WSLInterop.conf)" == 0 ]]; then
            echo ":WSLInterop:M::MZ::/init:PF" | sudo tee -a /usr/lib/binfmt.d/WSLInterop.conf
        else
            echo -e "WSLInterop.conf was already patched! ${PENGUIN} "
        fi

        echo
        echo "Restart your WSL with the following command: "
        echo -e "\twsl.exe --shutdown"
        echo -e "\nWSL successfully patched! ${PENGUIN} "
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ./unicode-emojis.sh
# shellcheck source=/dev/null
. ./os-check.sh
linuxb_wsl_systemd "$@"
