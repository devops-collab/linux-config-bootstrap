#!/bin/bash
function linuxb_minikube() {
    echo -e "\n${OCTOPUS} Kubernetes - minikube "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Kubernetes - minikube "
        echo "https://minikube.sigs.k8s.io/docs/start/"
        echo "https://minikube.sigs.k8s.io/docs/drivers/docker/"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            curl -L -o ~/minikube-darwin-amd64 https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-amd64
            sudo install ~/minikube-darwin-amd64 /usr/local/bin/minikube
            rm -v ~/minikube-darwin-amd64
            minikube config set driver docker
            return 0
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v minikube >/dev/null; then
            # Before_Script
            # curl
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi
            # Script
            curl -L -o ~/minikube-linux-amd64 https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
            sudo install ~/minikube-linux-amd64 /usr/local/bin/minikube
            rm -v ~/minikube-linux-amd64
            minikube config set driver docker
        else
            echo "Already present "
            minikube version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_minikube "$@"
